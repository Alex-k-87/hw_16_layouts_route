<?php


require_once '../vendor/autoload.php';
require_once '../config/blade.php';


$container = new \Illuminate\Container\Container();
$dispatcher = new \Illuminate\Events\Dispatcher($container);
$router = new \Illuminate\Routing\Router($dispatcher, $container);


//1:25
//1 variant
//$router->get('/', function () use($blade){

//   echo $blade->make('layouts.layout')->render();
//
//});
//2variant

//function view($view, array $data=[])   //Эта ф-ция перешла в config/blade.php
//{
//    global $blade;
//    return $blade->make($view,$data)->render();
//}
$router->get('/', \App\Controller\HomeController::class);
$router->get('/blog', \App\Controller\BlogController::class);
$router->get('/services', \App\Controller\ServicesController::class);
$router->get('/team', \App\Controller\TeamController::class );
$router->get('/contact', \App\Controller\ContactController::class );

$request = \Illuminate\Http\Request::createFromGlobals();
$router->dispatch($request);